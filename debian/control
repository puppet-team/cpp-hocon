Source: cpp-hocon
Priority: optional
Maintainer: Puppet Package Maintainers <pkg-puppet-devel@lists.alioth.debian.org>
Uploaders: Apollon Oikonomopoulos <apoikos@debian.org>
Build-Depends: debhelper-compat (= 13),
 cmake,
 libleatherman-dev,
 libboost-program-options-dev,
 catch (>= 1.10~),
Standards-Version: 4.5.0
Section: libs
Homepage: https://github.com/puppetlabs/cpp-hocon
Vcs-Git: https://salsa.debian.org/puppet-team/cpp-hocon.git
Vcs-Browser: https://salsa.debian.org/puppet-team/cpp-hocon

Package: libcpp-hocon-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends: libcpp-hocon0.3.0 (= ${binary:Version}), ${misc:Depends}
Description: C++ port of the TypesafeConfig library -- development files
 cpp-hocon is a port of the TypsafeConfig library to C++. It provides support
 for the HOCON configuration file format. HOCON aims to keep the semantics
 (tree structure; set of types; encoding/escaping) from JSON, but make it more
 convenient as a human-editable config file format.
 .
 This package contains cpp-hocon's development headers.

Package: libcpp-hocon0.3.0
Architecture: any
Multi-Arch: same
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: C++ port of the TypesafeConfig library -- shared libraries
 cpp-hocon is a port of the TypsafeConfig library to C++. It provides support
 for the HOCON configuration file format. HOCON aims to keep the semantics
 (tree structure; set of types; encoding/escaping) from JSON, but make it more
 convenient as a human-editable config file format.
 .
 This package contains the shared library.
